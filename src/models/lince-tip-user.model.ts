import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'lince_tip_user'}}
})
export class LinceTipUser extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'data_envio', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  dataEnvio?: string;

  @property({
    type: 'number',
    precision: 53,
    postgresql: {columnName: 'media', dataType: 'float', dataLength: null, dataPrecision: 53, dataScale: null, nullable: 'YES'},
  })
  media?: number;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'lince_usuario_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  linceUsuarioId: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'dica_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  dicaId: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceTipUser>) {
    super(data);
  }
}

export interface LinceTipUserRelations {
  // describe navigational properties here
}

export type LinceTipUserWithRelations = LinceTipUser & LinceTipUserRelations;
