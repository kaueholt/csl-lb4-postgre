import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: true,
    postgresql: {schema: 'public', table: 'lince_tip_user_answer'}
  }
})
export class LinceTipUserAnswer extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'resposta', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  resposta?: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'dicapergunta_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  dicaperguntaId: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'usuariodica_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  usuariodicaId: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceTipUserAnswer>) {
    super(data);
  }
}

export interface LinceTipUserAnswerRelations {
  // describe navigational properties here
}

export type LinceTipUserAnswerWithRelations = LinceTipUserAnswer & LinceTipUserAnswerRelations;
