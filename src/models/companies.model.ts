import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'public', table: 'companies'}}
})
export class Companies extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'name', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    length: 14,
    postgresql: {columnName: 'cnpj', dataType: 'character varying', dataLength: 14, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  cnpj: string;

  @property({
    type: 'string',
    required: true,
    length: 150,
    postgresql: {columnName: 'email', dataType: 'character varying', dataLength: 150, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  email: string;

  @property({
    type: 'string',
    required: true,
    length: 11,
    postgresql: {columnName: 'phone', dataType: 'character varying', dataLength: 11, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  phone: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'foundation_date', dataType: 'date', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  foundationDate: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'status', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  status: number;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Companies>) {
    super(data);
  }
}

export interface CompaniesRelations {
  // describe navigational properties here
}

export type CompaniesWithRelations = Companies & CompaniesRelations;
