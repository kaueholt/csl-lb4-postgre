import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'workers'}}
})
export class Workers extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    length: 250,
    postgresql: {columnName: 'avatar', dataType: 'character varying', dataLength: 250, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  avatar: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'name', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    length: 11,
    postgresql: {columnName: 'cpf', dataType: 'character varying', dataLength: 11, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  cpf: string;

  @property({
    type: 'string',
    required: true,
    length: 1,
    postgresql: {columnName: 'gender', dataType: 'character varying', dataLength: 1, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  gender: string;

  @property({
    type: 'string',
    required: true,
    length: 20,
    postgresql: {columnName: 'race', dataType: 'character varying', dataLength: 20, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  race: string;

  @property({
    type: 'string',
    required: true,
    length: 14,
    postgresql: {columnName: 'pis_nit', dataType: 'character varying', dataLength: 14, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  pisNit: string;

  @property({
    type: 'string',
    length: 12,
    postgresql: {columnName: 'phone', dataType: 'character varying', dataLength: 12, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  phone?: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {columnName: 'email', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  email: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'date_birth', dataType: 'date', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  dateBirth: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'status', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  status: number;

  @property({
    type: 'string',
    required: true,
    length: 20,
    postgresql: {columnName: 'role', dataType: 'character varying', dataLength: 20, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  role: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'rfid', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  rfid?: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'observation', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  observation?: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  updatedAt: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'company_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  companyId?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Workers>) {
    super(data);
  }
}

export interface WorkersRelations {
  // describe navigational properties here
}

export type WorkersWithRelations = Workers & WorkersRelations;
