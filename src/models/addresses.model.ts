import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: false, postgresql: {schema: 'public', table: 'addresses'}}
})
export class Addresses extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {columnName: 'street', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  street: string;

  @property({
    type: 'string',
    required: true,
    length: 4,
    postgresql: {columnName: 'number', dataType: 'character varying', dataLength: 4, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  number: string;

  @property({
    type: 'string',
    length: 50,
    postgresql: {columnName: 'complement', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  complement?: string;

  @property({
    type: 'string',
    required: true,
    length: 150,
    postgresql: {columnName: 'district', dataType: 'character varying', dataLength: 150, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  district: string;

  @property({
    type: 'string',
    required: true,
    length: 255,
    postgresql: {columnName: 'city', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  city: string;

  @property({
    type: 'string',
    required: true,
    length: 2,
    postgresql: {columnName: 'state', dataType: 'character varying', dataLength: 2, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  state: string;

  @property({
    type: 'string',
    required: true,
    length: 8,
    postgresql: {columnName: 'zipcode', dataType: 'character varying', dataLength: 8, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  zipcode: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: true,
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  updatedAt: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'company_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  companyId?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Addresses>) {
    super(data);
  }
}

export interface AddressesRelations {
  // describe navigational properties here
}

export type AddressesWithRelations = Addresses & AddressesRelations;
