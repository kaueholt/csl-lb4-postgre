import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: true,
    postgresql: {schema: 'public', table: 'lince_tip_question'}
  }
})
export class LinceTipQuestion extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'pergunta', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  pergunta: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'peso', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  peso?: number;

  @property({
    type: 'string',
    postgresql: {columnName: 'resposta', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  resposta?: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'tipo_resposta_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  tipoRespostaId: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceTipQuestion>) {
    super(data);
  }
}

export interface LinceTipQuestionRelations {
  // describe navigational properties here
}

export type LinceTipQuestionWithRelations = LinceTipQuestion & LinceTipQuestionRelations;
