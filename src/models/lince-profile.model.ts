import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'lince_profile'}}
})
export class LinceProfile extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'descricao', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  descricao?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceProfile>) {
    super(data);
  }
}

export interface LinceProfileRelations {
  // describe navigational properties here
}

export type LinceProfileWithRelations = LinceProfile & LinceProfileRelations;
