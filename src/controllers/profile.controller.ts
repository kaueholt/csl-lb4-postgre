// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';

import {repository} from '@loopback/repository';
import {
  get,

  getModelSchemaRef, param
} from '@loopback/rest';
import {LinceProfile, LinceUser, Users} from '../models';
import {LinceUserRepository} from '../repositories';

export class ProfileController {
  constructor(
    @repository(LinceUserRepository)
    public linceUserRepository: LinceUserRepository,
  ) {}

  @get('/linkUsers', {
    responses: {
      '200': {
        description: 'Recupera informações do usuário Lince no banco SESI. (O usuário-pai é do SESI, e portanto suas informações pessoais devem ser recuperadas a partir do SESI.)',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Users, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async linkUsers(): Promise<LinceUser[]> {
    let sqlStmt = "SELECT lince_user.id as LINCE_id, " +
      "users.id as SESI_id, users.avatar as SESI_avatar, users.name as SESI_name, users.cpf as SESI_cpf, " +
      "users.password as SESI_password, users.token as SESI_token, users.email as SESI_email, users.date_birth as SESI_date_birth, " +
      "users.status as SESI_status, users.role as SESI_role, users.created_at as SESI_created_at, updated_at as SESI_updated_at " +
      "FROM lince_user LEFT JOIN users ON lince_user.sesi_usuario_id = users.id";
    return this.linceUserRepository.dataSource.execute(sqlStmt);
  }

  @get('/perfis/usuario/{id}', {
    responses: {
      '200': {
        description: 'Recupera informações dos perfís do usuário LINCE',
        content: {
          'application/json': {
            schema: getModelSchemaRef(LinceProfile, {includeRelations: true}),
          },
        },
      },
    },
  })
  async getUserProfiles(@param.path.string('id') id: string): Promise<LinceProfile[]> {
    let sqlStmt = "SELECT lince_user.*, lince_profile.descricao, lince_user_profile.origem_cadastro " +
      "FROM lince_user_profile " +
      "LEFT JOIN lince_profile ON lince_user_profile.perfil_id = lince_profile.id " +
      "LEFT JOIN lince_user ON lince_user_profile.lince_usuario_id = lince_user.id " +
      "WHERE lince_user.status > 0  " +
      "AND lince_user.id = '" + id + "'"
    return this.linceUserRepository.dataSource.execute(sqlStmt);
  }
}
