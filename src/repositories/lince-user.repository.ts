import {DefaultCrudRepository} from '@loopback/repository';
import {LinceUser, LinceUserRelations} from '../models';
import {CslposgremarvinDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceUserRepository extends DefaultCrudRepository<
  LinceUser,
  typeof LinceUser.prototype.id,
  LinceUserRelations
> {
  constructor(
    @inject('datasources.cslposgremarvin') dataSource: CslposgremarvinDataSource,
  ) {
    super(LinceUser, dataSource);
  }
}
