import {DefaultCrudRepository} from '@loopback/repository';
import {LinceProfile, LinceProfileRelations} from '../models';
import {CslposgremarvinDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceProfileRepository extends DefaultCrudRepository<
  LinceProfile,
  typeof LinceProfile.prototype.id,
  LinceProfileRelations
> {
  constructor(
    @inject('datasources.cslposgremarvin') dataSource: CslposgremarvinDataSource,
  ) {
    super(LinceProfile, dataSource);
  }
}
