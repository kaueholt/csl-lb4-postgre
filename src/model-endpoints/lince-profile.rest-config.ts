import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceProfile} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceProfile,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-profiles',
};
module.exports = config;
