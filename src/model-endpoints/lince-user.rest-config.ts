import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceUser} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceUser,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-users',
};
module.exports = config;
