import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Companies} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Companies,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/companies',
};
module.exports = config;
