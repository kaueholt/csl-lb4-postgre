import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceWebhook} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceWebhook,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-webhooks',
};
module.exports = config;
