import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Addresses} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Addresses,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/addresses',
};
module.exports = config;
