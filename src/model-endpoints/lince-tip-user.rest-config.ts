import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceTipUser} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceTipUser,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-tip-users',
};
module.exports = config;
