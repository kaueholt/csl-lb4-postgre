import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceBundlingParams} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceBundlingParams,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-bundling-params',
};
module.exports = config;
