import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceTipUserAnswer} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceTipUserAnswer,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-tip-user-answers',
};
module.exports = config;
