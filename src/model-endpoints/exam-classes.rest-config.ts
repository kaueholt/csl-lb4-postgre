import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {ExamClasses} from '../models';

const config: ModelCrudRestApiConfig = {
  model: ExamClasses,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/exam-classes',
};
module.exports = config;
