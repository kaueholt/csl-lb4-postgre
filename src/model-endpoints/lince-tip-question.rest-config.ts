import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceTipQuestion} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceTipQuestion,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-tip-questions',
};
module.exports = config;
