import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Users} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Users,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/users',
};
module.exports = config;
