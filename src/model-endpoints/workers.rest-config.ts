import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {Workers} from '../models';

const config: ModelCrudRestApiConfig = {
  model: Workers,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/workers',
};
module.exports = config;
