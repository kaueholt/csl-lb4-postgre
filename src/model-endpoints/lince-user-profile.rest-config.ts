import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceUserProfile} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceUserProfile,
  pattern: 'CrudRest',
  dataSource: 'cslposgremarvin',
  basePath: '/lince-user-profiles',
};
module.exports = config;
